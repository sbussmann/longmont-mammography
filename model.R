##############################################################################
# Project: Longmont Mammography
# Date: 10/1/2014
# Author: Sam Bussmann
# Description: Build Model
# Notes: 
##############################################################################

## Undersample non cases -- sample according to client distribution

#nonhk<-data.frame(id=sample.int(length(cm$pt_flag),100*sum(cm$pt_flag)))
nonhk<-data.frame(id=sample.int(length(cm$pt_flag),length(cm$pt_flag)))
hk<-data.frame(id=which(cm$pt_flag==1))
allhk<-merge(nonhk,hk,by=c("id"),all=T)
save(allhk,file="allhk.RData")
#load("allhk.RData")

### Fit model using glmnet ###
library(parallel)
library(doSNOW)
library(foreach)


## OwnRent did nothing, remove it from interactions. Also spouse indicator and income did nothing. remove it.
system.time(
  dev.mod<-sparse.model.matrix(as.factor(pt_flag)~ . ,
                               data=cm,
                               contrasts.arg = lapply(cm[,sapply(cm, is.factor)], contrasts, contrasts=FALSE))
)
# system.time(
#   dev.mod<-sparse.model.matrix(as.factor(pt_flag)~ . + spouseindicator.bin:findr.bin 
#                                + Gender:Age + Age:findr.bin 
#                                + MinDist.std:work_private_vehicle.std,
#                                data=cm,
#                                contrasts.arg = lapply(cm[,sapply(cm, is.factor)], contrasts, contrasts=FALSE))
# )


save(dev.mod,file="dev.mod.Rdata")
#load("dev.mod.Rdata")

### Testing LASSO vs Ridge / Elastic Net

cl<-makeCluster(2) 
registerDoSNOW(cl)

#foldid <- sample(1:10, size = length(cm$pt_flag), replace = TRUE)
foldid <- sample(1:10, size = length(allhk$id), replace = TRUE)
system.time(
  cvnet1.p1<-cv.glmnet(dev.mod[allhk$id,],cm$pt_flag[allhk$id], family="binomial",type.measure="deviance",
                       standardize=F,parallel = TRUE,foldid = foldid, alpha = 1)
)
system.time(
  cvnet1.p5<-cv.glmnet(dev.mod[allhk$id,],cm$pt_flag[allhk$id], family="binomial",type.measure="deviance",
                       standardize=F,parallel = TRUE,foldid = foldid, alpha = .5)
)
system.time(
  cvnet1.p0<-cv.glmnet(dev.mod[allhk$id,],cm$pt_flag[allhk$id], family="binomial",type.measure="deviance",
                       standardize=F,parallel = TRUE,foldid = foldid, alpha = .01)
)

stopCluster(cl)

mean((abs(cvnet1.p1$glmnet.fit$beta[,which(cvnet1.p1$lambda==cvnet1.p1$lambda.min)]) > .05))
mean((abs(cvnet1.p5$glmnet.fit$beta[,which(cvnet1.p5$lambda==cvnet1.p5$lambda.min)]) > .05))
mean((abs(cvnet1.p0$glmnet.fit$beta[,which(cvnet1.p0$lambda==cvnet1.p0$lambda.min)]) > .05))

save(cvnet1.p1,file="model_cvnet1.p1.Rdata")
#load("model_cvnet1.p1.Rdata")
save(cvnet1.p5,file="model_cvnet1.p5.Rdata")
save(cvnet1.p0,file="model_cvnet1.p0.Rdata")

min(cvnet1.p1$cvm)
min(cvnet1.p5$cvm)
min(cvnet1.p0$cvm)

which(cvnet1.p1$cvm==min(cvnet1.p1$cvm))
which(cvnet1.p5$cvm==min(cvnet1.p5$cvm))
which(cvnet1.p0$cvm==min(cvnet1.p0$cvm))


par(mfrow=c(1,1))
plot(cvnet1.p1$glmnet.fit)
plot(cvnet1.p1$glmnet.fit,xvar="dev")
plot(cvnet1.p1$glmnet.fit,xvar="lambda")


par(mfrow = c(1, 1))
plot(cvnet1.p1)
plot(cvnet1.p5)
plot(cvnet1.p0)
plot(log(cvnet1.p1$lambda), cvnet1.p1$cvm, pch = 19, col = "red", xlab = "log(Lambda)", 
     ylab = cvnet1.p1$name)
points(log(cvnet1.p5$lambda), cvnet1.p5$cvm, pch = 19, col = "grey")
points(log(cvnet1.p0$lambda), cvnet1.p0$cvm, pch = 19, col = "blue")
legend("top", legend = c("alpha = 1", "alpha = .5", "alpha = 0"), pch = 19, 
       col = c("red", "grey", "blue"))

coef(cvnet1.p1,s = "lambda.min")


pred.prop<-predict(cvnet1.p1,dev.mod,#[cm$ClientName=="Claxton-Hepburn Medical Center",],
                   s = "lambda.min",type="response")

save(pred.prop,file="pred.prop.Rdata")
#load("pred.prop.Rdata")
